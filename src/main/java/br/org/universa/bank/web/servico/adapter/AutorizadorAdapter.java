package br.org.universa.bank.web.servico.adapter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import br.org.universa.bank.autorizador.negocio.autorizacao.Autorizacao;
import br.org.universa.bank.autorizador.negocio.autorizacao.CanalDeAtendimento;
import br.org.universa.bank.autorizador.negocio.autorizacao.EstadoDaAutorizacao;
import br.org.universa.bank.autorizador.negocio.conta.Conta;
import br.org.universa.bank.autorizador.negocio.conta.LancamentoDaConta;
import br.org.universa.bank.autorizador.negocio.fundos.TipoDoFundo;
import br.org.universa.bank.autorizador.negocio.transacao.TipoDaTransacao;
import br.org.universa.bank.autorizador.negocio.transacao.Transacao;
import br.org.universa.bank.autorizador.servico.AutorizadorFacade;
import br.org.universa.bank.web.servico.vo.ComprovanteVO;
import br.org.universa.bank.web.servico.vo.ContaVO;
import br.org.universa.bank.web.servico.vo.LancamentoDaContaVO;
import br.org.universa.bank.web.servico.vo.TransacaoVO;

public class AutorizadorAdapter {

	private static AutorizadorAdapter instancia = null;

	private AutorizadorAdapter() {
		// Construtor privado
	}

	public static AutorizadorAdapter get() {
		if (instancia == null) {
			instancia = new AutorizadorAdapter();
		}

		return instancia;
	}

	public ContaVO consultaConta(Integer agencia, Integer numero)
			throws Exception {
		Conta conta = AutorizadorFacade.get().consultaConta(agencia, numero);
		ContaVO contaVO = new ContaVO();
		contaVO.setAgencia(conta.getAgencia());
		contaVO.setCpfDoTitular(conta.getTitular().getCpf());
		contaVO.setNumero(conta.getNumero());
		contaVO.setSaldo(new BigDecimal(conta.getSaldo()));
		contaVO.setTitular(conta.getTitular().getNome());
		for (LancamentoDaConta lancamento : conta.getLancamentosDaConta()){
			contaVO.adicionaLancamentoDaConta(criaLancamentoDaConta(lancamento));
		}
		return contaVO;
	}

	public ComprovanteVO deposita(ContaVO conta, BigDecimal valor)
			throws Exception {
		Transacao transacao = new Transacao();
		transacao.setAgencia(conta.getAgencia());
		transacao.setCanalDeAtendimento(CanalDeAtendimento.INTERNET_BANKING);
		transacao.setConta(conta.getNumero());
		transacao.setTipo(TipoDaTransacao.DEPOSITO_EM_CONTA);
		transacao.setValor(valor.doubleValue());
		Autorizacao autorizacao = AutorizadorFacade.get().executa(transacao);
		ComprovanteVO comprovanteVO = criaComprovante(autorizacao);
		return  comprovanteVO;
	}

	public ComprovanteVO saca(ContaVO conta, BigDecimal valor) throws Exception {
		// TODO - Implementar

		return null;
	}

	public ComprovanteVO transfere(ContaVO conta, BigDecimal valor,
			Integer agenciaFavorecida, Integer contaFavorecida)
			throws Exception {
		// TODO - Implementar

		return null;
	}

	public ComprovanteVO investeEmFundo(ContaVO conta, BigDecimal valor,
			TipoDoFundo tipoDoFundo) throws Exception {
		// TODO - Implementar

		return null;
	}

	public ComprovanteVO realizaDocTed(ContaVO conta, BigDecimal valor,
			Integer bancoFavorecido, Integer agenciaFavorecida,
			Integer contaFavorecida, String cpfDoTitularDaContaFavorecida)
			throws Exception {
		// TODO - Implementar

		return null;
	}

	public List<TransacaoVO> consultaTransacoes(Date dataDeReferencia) {
		// TODO - Implementar

		return null;
	}

	@SuppressWarnings("unused")
	private ComprovanteVO executaTransacao(Transacao transacao)
			throws Exception {
		Autorizacao autorizacao = AutorizadorFacade.get().executa(transacao);

		analisaRetornoDaAutorizacao(autorizacao);

		return criaComprovante(autorizacao);
	}

	private void analisaRetornoDaAutorizacao(Autorizacao autorizacao)
			throws Exception {
		if (autorizacao.getEstado().getChave()
				.equals(EstadoDaAutorizacao.NEGADA.getChave())) {
			throw new Exception(autorizacao.getMotivoDaNegacao());
		}
	}

	private ComprovanteVO criaComprovante(Autorizacao autorizacao) {
		ComprovanteVO comprovanteVO = new ComprovanteVO();

		comprovanteVO.setTipoDaTransacao(autorizacao.getTransacao().getTipo()
				.getValor());
		comprovanteVO.setAgencia(autorizacao.getTransacao().getAgencia());
		comprovanteVO.setConta(autorizacao.getTransacao().getConta());
		comprovanteVO.setValor(new BigDecimal(autorizacao.getTransacao()
				.getValor()));
		comprovanteVO.setDataHoraTransacao(autorizacao.getTransacao()
				.getDataHoraCriacao());
		comprovanteVO.setIdDaTransacao(autorizacao.getTransacao()
				.getIdentificador());

		return comprovanteVO;
	}

	@SuppressWarnings("unused")
	private LancamentoDaContaVO criaLancamentoDaConta(
			LancamentoDaConta lancamento) {

		return new LancamentoDaContaVO(lancamento.getDataDoLancamento(),
				lancamento.getTipoDoLancamento(), lancamento.getDescricao(),
				new BigDecimal(lancamento.getValor()));
	}
}