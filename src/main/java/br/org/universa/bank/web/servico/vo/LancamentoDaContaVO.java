package br.org.universa.bank.web.servico.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import br.org.universa.bank.autorizador.negocio.conta.TipoDoLancamento;

public class LancamentoDaContaVO implements Serializable {

	private static final long serialVersionUID = -2754597026447632736L;

	private Date dataDoLancamento;
	private TipoDoLancamento tipoDoLancamento;
	private String descricao;
	private BigDecimal valor;

	public LancamentoDaContaVO(Date dataDoLancamento,
			TipoDoLancamento tipoDoLancamento, String descricao,
			BigDecimal valor) {
		this.dataDoLancamento = dataDoLancamento;
		this.tipoDoLancamento = tipoDoLancamento;
		this.descricao = descricao;
		this.valor = valor;
	}

	public Date getDataDoLancamento() {
		return dataDoLancamento;
	}

	public void setDataDoLancamento(Date dataDoLancamento) {
		this.dataDoLancamento = dataDoLancamento;
	}

	public TipoDoLancamento getTipoDoLancamento() {
		return tipoDoLancamento;
	}

	public void setTipoDoLancamento(TipoDoLancamento tipoDoLancamento) {
		this.tipoDoLancamento = tipoDoLancamento;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
}
